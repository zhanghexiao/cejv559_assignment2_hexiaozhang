
package com.mycompany.beans;

import static com.mycompany.beans.getProperty.context;
import static com.mycompany.beans.getProperty.pp;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author zhanghexiao
 */

//This method is used to change the language.
        
@Named("localeChoose")
@RequestScoped
public class LocaleChoose {

    @Inject
    CustomerBean customers;

    static InputStream fps;
    static Properties pp;

    public String changeLocale() {
        this.pp = new Properties();

        FacesContext context = FacesContext.getCurrentInstance();
        String languageCode = getLanguageCode(context);
        customers.setLanguage(languageCode);
        Locale aLocale;
        switch (languageCode) {
            case "en_CA":
                aLocale = Locale.CANADA;
                break;
            case "fr_CA":
                aLocale = Locale.CANADA_FRENCH;
                break;
            default:
                aLocale = Locale.getDefault();
        }
        context.getViewRoot().setLocale(aLocale);

   
        return languageCode;
    }

    private String getLanguageCode(FacesContext context) {
        Map<String, String> params = context.getExternalContext()
                .getRequestParameterMap();
        return params.get("languageCode");
    }

}
