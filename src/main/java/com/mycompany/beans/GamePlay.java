/**
 * @author zhanghexiao
 */
package com.mycompany.beans;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Properties;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;


@Named("play")
@RequestScoped
public class GamePlay {

    @Inject
    CustomerBean customers;
    int rollingResult;

    public GamePlay() {
        super();
    }

//    When the customer login, call setInitialBalanceUpdate to set the bankroll
    
    public String setInitialBalanceUpdate() {
        BigDecimal temp = customers.getInitialBalance();
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        //////////       
        ////////// 
        customers.setInitialBalance(temp);
        customers.setCurrentBalance(temp);
        return "playPage";
    }

//    use Interger Type to calculate the bet result in three different game
    
    public String balanceUpdate(int type) {

        BigDecimal balanceChange;
        BigDecimal betResult;
        BigDecimal zero = new BigDecimal("0.00");
        rolling();

        switch (type) {
            case 1:
                if (ifWin_PassLine() == 0) {
                    customers.setPoint(this.rollingResult);
                    customers.setEachRollResult("");
                    return "pointPage";
                }
                balanceChange = customers.getBetAmount1().multiply(new BigDecimal(ifWin_PassLine()));
                break;
            case 2:
                balanceChange = customers.getBetAmount2().multiply(new BigDecimal(ifWin_FieldBet()));
                break;
            case 3:
                balanceChange = customers.getBetAmount3().multiply(new BigDecimal(ifWin_Any7()));
                break;
            default:
                betResult = new BigDecimal("-10000.00");
                balanceChange = new BigDecimal("-10000.00");
        }

// When get the balanceChange, the current bankroll can also be calculated.       
        
        resultToUser(balanceChange);
        rollResultToUser();
        BigDecimal temp = balanceChange.add(customers.getCurrentBalance());
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        customers.setCurrentBalance(temp);

//Every time the balanced is updated, check if the customer still have money to bet.        
        
        if (customers.getCurrentBalance().equals(zero)) {
            return "end";
        } else {
            return null;
        }

    }
    
  
    public void rolling() {
        int firstRoll;
        int secondRoll;
        firstRoll = (int) (Math.random() * 6) + 1;
        secondRoll = (int) (Math.random() * 6) + 1;
        customers.setOutPutResult1(String.valueOf(firstRoll));
        customers.setOutPutResult2(String.valueOf(secondRoll));
        this.rollingResult = firstRoll + secondRoll;
    }

//The rolling Again method it used in the PassLine game. And get the new rolling result to
//    compare to the previous rolling results.
    
    public int rollingAgain() {
        int firstRoll;
        int secondRoll;
        firstRoll = (int) (Math.random() * 6) + 1;
        secondRoll = (int) (Math.random() * 6) + 1;
        customers.setOutPutResult1(String.valueOf(firstRoll));
        customers.setOutPutResult2(String.valueOf(secondRoll));
        rollResultToUser();
        return firstRoll + secondRoll;
    }
    
//These three methods are genereate the bet results according to the Rules.  

    public int ifWin_PassLine() {
     
        switch (this.rollingResult) {
            case 7:
            case 11:
                return 1;
            case 2:
            case 3:
            case 12:
                return -1;
            default:
                return 0;
        }
    }

    public int ifWin_FieldBet() {
        switch (this.rollingResult) {
            case 3:
            case 4:
            case 9:
            case 10:
            case 11:
                return 1;
            case 2:
                return 2;
            case 12:
                return 3;
            default:
                return -1;
        }
    }

    public int ifWin_Any7() {
        switch (this.rollingResult) {
            case 7:
                return 4;
            default:
                return -1;
        }
    }

 
//PassLinePoint Method is used when the first roll in point.    
    
    public String PassLinePoint() {
        int tempRoll;
        BigDecimal balanceChange = new BigDecimal("0.00");
        BigDecimal betResult;
        BigDecimal zero = new BigDecimal("0.00");
        tempRoll = rollingAgain();
        
        if (tempRoll == 7 || tempRoll == this.rollingResult) {
            if (tempRoll == this.rollingResult) {
                betResult = new BigDecimal(1);
                balanceChange = customers.getBetAmount1().multiply(betResult);
                resultToUser(balanceChange);
            }
            if (tempRoll == 7) {
                betResult = new BigDecimal(-1);
                balanceChange = customers.getBetAmount1().multiply(betResult);
                resultToUser(balanceChange);
            }           
            BigDecimal temp = balanceChange.add(customers.getCurrentBalance());
            temp = temp.setScale(2, RoundingMode.HALF_EVEN);
            customers.setCurrentBalance(temp);
            if (customers.getCurrentBalance().equals(zero)) {
                return "end";
            } else {
                return "PassLine";
            }
        } else {
            return "pointPage";
        }

    }

///These two methods are used to generate the results, which is shown to the customers.  
    
    public void resultToUser(BigDecimal balanceChange) {
        BigDecimal zero = new BigDecimal("0.00");
        StringBuilder tempResult = new StringBuilder();
        if (balanceChange.compareTo(zero) == 1) {
            tempResult.append(getProperty.getInformation("youWin", customers.getLanguage()));
        }
        if (balanceChange.compareTo(zero) == -1) {
            tempResult.append(getProperty.getInformation("youLose", customers.getLanguage()));
        }
        tempResult.append(" $");
        tempResult.append(balanceChange.abs().toString());
        customers.setBetOutput(tempResult.toString());

    }

    public void rollResultToUser() {
        StringBuilder tempResult = new StringBuilder();
       
        tempResult.append(getProperty.getInformation("youRoll", customers.getLanguage()));       
        tempResult.append(" ");
        tempResult.append(customers.getOutPutResult1());
        tempResult.append(" and ");
        tempResult.append(customers.getOutPutResult2());
        customers.setEachRollResult(tempResult.toString());

    }
    
        public String endCurrentGame() {
        customers.setEachRollResult("");
        customers.setBetOutput("");
        return "playPage";
    }

}
