/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 * @author zhanghexiao
 */
@Named("customers")
@SessionScoped
public class CustomerBean implements Serializable {

//    betAmount1,2,3 is for three game.
//    outPutResult is the random generated to roll the dices
//    betOutput, eachRollResult are shown the win or lose the amount of money and the roll dice
 
    
    private String name;
    private String mail;
    private BigDecimal initialBalance;
    private BigDecimal currentBalance;
    private BigDecimal betAmount1;
    private BigDecimal betAmount2;
    private BigDecimal betAmount3;
    private String outPutResult1;
    private String outPutResult2;
    private String betOutput = "";
    private String eachRollResult = "";
    private int point;
    private String language = "";

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEachRollResult() {
        return eachRollResult;
    }

    public void setEachRollResult(String eachRollResult) {
        this.eachRollResult = eachRollResult;
    }

    public String getBetOutput() {
        return betOutput;
    }

    public void setBetOutput(String betOutput) {
        this.betOutput = betOutput;
    }

    public String getOutPutResult1() {
        return outPutResult1;
    }

    public void setOutPutResult1(String outPutResult) {
        this.outPutResult1 = outPutResult;
    }

    public String getOutPutResult2() {
        return outPutResult2;
    }

    public void setOutPutResult2(String outPutResult) {
        this.outPutResult2 = outPutResult;
    }

    public BigDecimal getBetAmount1() {
        return betAmount1;
    }

    public void setBetAmount1(BigDecimal betAmount) {
        this.betAmount1 = betAmount;
    }

    public BigDecimal getBetAmount2() {
        return betAmount2;
    }

    public void setBetAmount2(BigDecimal betAmount2) {
        this.betAmount2 = betAmount2;
    }

    public BigDecimal getBetAmount3() {
        return betAmount3;
    }

    public void setBetAmount3(BigDecimal betAmount3) {
        this.betAmount3 = betAmount3;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal betAmount) {
        this.initialBalance = betAmount;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void validateInitalAmount(FacesContext context, UIComponent component,
            Object value) {
        BigDecimal amount;
        //BigDecimal amount = new BigDecimal((Double) value);
        if (value instanceof BigDecimal) {
            amount = new BigDecimal(value.toString());
        } else {
            amount = new BigDecimal(0);
        }
        int result = amount.compareTo(new BigDecimal(10));
        if (result == -1 || result == 0) {
            FacesMessage message = com.mycompany.util.Message.getMessage(
                    "com.hexiaozhang.bundles.messages", "littleMoney", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
        int result2 = amount.compareTo(new BigDecimal(10000));
        if (result2 == 1) {
            FacesMessage message = com.mycompany.util.Message.getMessage(
                    "com.hexiaozhang.bundles.messages", "muchMoney", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }

    }

    public void validateBetAmount(FacesContext context, UIComponent component,
            Object value) {
        BigDecimal amount;
        //BigDecimal amount = new BigDecimal((Double) value);
        if (value instanceof BigDecimal) {
            amount = new BigDecimal(value.toString());
        } else {
            amount = new BigDecimal(0);
        }
        int result1 = amount.compareTo(new BigDecimal(1));
        int result2 = amount.compareTo(this.currentBalance);
        if (result1 == -1 || result1 == 0) {
            FacesMessage message = com.mycompany.util.Message.getMessage(
                    "com.hexiaozhang.bundles.messages", "lessBetMoney", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
        if (result2 == 1) {
            FacesMessage message = com.mycompany.util.Message.getMessage(
                    "com.hexiaozhang.bundles.messages", "muchBetMoney", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }

    }

}
