
/**
 * @author zhanghexiao
 */

package com.mycompany.beans;

import java.io.InputStream;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named("getProperty")
@RequestScoped


//This method is used to get the string of different language to output bet result to customers

public class getProperty {
    
        static FacesContext context = FacesContext.getCurrentInstance();              
	static Properties pp;
        static String languageCode = getLanguageCode(context);
           
	public static String getInformation(String key,String language) {
                		pp = new Properties();
                InputStream fps;
		try {   
 ////////////////////////////////////////////////////                   
         switch (language) {
            case "en_CA":
                fps = getProperty.class.getResourceAsStream("/com/hexiaozhang/bundles/messages_en_CA.properties");
                break;
            case "fr_CA":
                fps = getProperty.class.getResourceAsStream("/com/hexiaozhang/bundles/messages_fr_CA.properties");
                break;
            default:
                fps = getProperty.class.getResourceAsStream("/com/hexiaozhang/bundles/messages.properties");
        }

  //////////////////////////////////////////////////////////////////  //////////////                                       
//			fps = getProperty.class.getResourceAsStream("/com/hexiaozhang/bundles/messages.properties");
			pp.load(fps);
			fps.close();
		} catch (Exception e) {
			System.out.print("Failed to load the property!");
			e.printStackTrace();
		}
		String value = pp.getProperty(key);
		if (value != null) {
			return value;
		} else {
			return null;
		}
	}
        
        private static String getLanguageCode(FacesContext context) {
        Map<String, String> params = context.getExternalContext()
                .getRequestParameterMap();
        return params.get("languageCode");
    }
}

