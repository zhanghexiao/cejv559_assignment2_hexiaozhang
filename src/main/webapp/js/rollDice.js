function g(id) {
    return document.getElementById(id)
}

function on(dom, event, func) {
    if (dom.addEventListener) {
        dom.addEventListener(event, func, false)
    } else {
        dom.attachEvent('on' + event, func)
    }
}

var myDice1 = dice(document.getElementById('area1'), {
    // edgeLength:100
    // ,radius:5
    // ,
    // hasShadow:false,
    // shadowTop:90,
    // keepAnimationTime:10000,
    onKeepAnimationEnd: function (result) {
        console.log('onKeepAnimationEnd,result:' + result + ',time:' + (new Date).getTime())
    }
    , onEndAnimationEnd: function (result) {
        console.log('onEndAnimationEnd,result:' + result + ',time:' + (new Date).getTime())
    }
})
on(g('roll'), 'click', function () {
    myDice1.roll(g('result_input1').value.split(','))
})


var myDice2 = dice(document.getElementById('area2'), {
    // edgeLength:100
    // ,radius:5
    // ,
    // hasShadow:false,
    // shadowTop:90,
    // keepAnimationTime:10000,
    onKeepAnimationEnd: function (result) {
        console.log('onKeepAnimationEnd,result:' + result + ',time:' + (new Date).getTime())
    }
    , onEndAnimationEnd: function (result) {
        console.log('onEndAnimationEnd,result:' + result + ',time:' + (new Date).getTime())
    }
})
on(g('roll'), 'click', function () {
    myDice2.roll(g('result_input2').value.split(','))
})

function autoRoll(){
myDice1.roll(g('result_input1').value.split(','))    
myDice2.roll(g('result_input2').value.split(','))   
}


